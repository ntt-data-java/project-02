package com.nttdata.jjla.banco.repository;

import com.nttdata.jjla.banco.entity.Banco;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BancoRepository extends JpaRepository<Banco, Long> {
}
