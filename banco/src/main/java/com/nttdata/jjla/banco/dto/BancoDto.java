package com.nttdata.jjla.banco.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BancoDto {

    private Long id;
    private String nombre;
    private String direccion;
    private Boolean active;

}
