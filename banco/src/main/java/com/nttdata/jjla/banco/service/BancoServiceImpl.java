package com.nttdata.jjla.banco.service;

import com.nttdata.jjla.banco.dto.BancoDto;
import com.nttdata.jjla.banco.entity.Banco;
import com.nttdata.jjla.banco.model.Cuentabancaria;
import com.nttdata.jjla.banco.repository.BancoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BancoServiceImpl implements IBancoService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    BancoRepository bancoRepository;
    @Autowired
    ModelMapper modelMapper;


    @Override
    public BancoDto registrar(BancoDto bancoDto) {
        Banco registrado = bancoRepository.save(mapearEntidad(bancoDto));
        return mapearDTO(registrado);
    }

    @Override
    public BancoDto actualizar(Long id, BancoDto bancoDto) {
        Banco banco = bancoRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        banco.setNombre(bancoDto.getNombre());
        banco.setDireccion(bancoDto.getDireccion());
        banco.setActive(bancoDto.getActive());
        Banco actualizado = bancoRepository.save(banco);
        return mapearDTO(actualizado);
    }

    @Override
    public List<BancoDto> listar() {
        List<Banco> bancos = bancoRepository.findAll();
        return bancos.stream().map(banco -> mapearDTO(banco)).collect(Collectors.toList());
    }

    @Override
    public BancoDto buscar(Long id) {
        Optional<Banco> bancoFound = bancoRepository.findById(id);
        return bancoFound.map(banco ->mapearDTO(banco)).orElseThrow(() -> new RuntimeException("Valor no presente"));
    }

    @Override
    public String eliminar(Long id) {
        Banco banco = bancoRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        if (Objects.nonNull(banco)) {
            bancoRepository.deleteById(id);
            return "Eliminacion exitosa";
        }
        return "No existe el registro";
    }

    // ****************** REST TEMPLATE ***************
    @Override
    public List<Cuentabancaria> obtenerCuentasBancarias(Long bancoId) {
        List<Cuentabancaria> cuentaBancarias = restTemplate.getForObject("http://localhost:8004/api/cuenta/banco/" + bancoId, List.class);
        return cuentaBancarias;
    }

    /*************** MAPEO CON MODELMAPPER *******************/
    private Banco mapearEntidad(BancoDto bancoDto){
        Banco entidadMapeada = modelMapper.map(bancoDto, Banco.class);
        return entidadMapeada;
    }
    private BancoDto mapearDTO(Banco banco){
        BancoDto DTOmapeado = modelMapper.map(banco,BancoDto.class);
        return DTOmapeado;
    }


}
