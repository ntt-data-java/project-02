package com.nttdata.jjla.banco.controller;

import com.nttdata.jjla.banco.dto.BancoDto;
import com.nttdata.jjla.banco.model.Cuentabancaria;
import com.nttdata.jjla.banco.service.BancoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/banco")
public class BancoController {

    @Autowired
    BancoServiceImpl bancoService;

    @GetMapping("/listar")
    public List<BancoDto> listar() {
        return bancoService.listar();
    }

    @PostMapping("/registrar")
    public BancoDto registrar(@RequestBody BancoDto bancoDto) {
        return bancoService.registrar(bancoDto);
    }

    @PutMapping("/actualizar/{id}")
    public BancoDto actualizar(@PathVariable Long id,@RequestBody BancoDto bancoDto){
        return bancoService.actualizar(id, bancoDto);
    }

    @GetMapping("/buscar/{id}")
    public BancoDto buscar(@PathVariable("id") long id){
        return bancoService.buscar(id);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id) {
        return bancoService.eliminar(id);
    }

    // *************************** REST TEMPLATE *************************
    @GetMapping("/cuenta/{bancoId}")
    public ResponseEntity<List<Cuentabancaria>> obtenerCuentaBancaria(@PathVariable("bancoId") Long bancoId){
        BancoDto banco = bancoService.buscar(bancoId);
        if (banco == null) {
            return ResponseEntity.notFound().build();
        }
        List<Cuentabancaria> cuentaBancarias = bancoService.obtenerCuentasBancarias(bancoId);
        return ResponseEntity.ok(cuentaBancarias);
    }

}
