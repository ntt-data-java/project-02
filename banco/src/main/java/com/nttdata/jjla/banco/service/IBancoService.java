package com.nttdata.jjla.banco.service;

import com.nttdata.jjla.banco.dto.BancoDto;
import com.nttdata.jjla.banco.model.Cuentabancaria;

import java.util.List;

public interface IBancoService {

    BancoDto registrar(BancoDto bancoDto);
    BancoDto actualizar(Long id, BancoDto bancoDto);
    List<BancoDto> listar();
    String eliminar(Long id);
    BancoDto buscar(Long id);

    List<Cuentabancaria> obtenerCuentasBancarias(Long bancoId);

}
