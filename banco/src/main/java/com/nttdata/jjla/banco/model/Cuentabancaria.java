package com.nttdata.jjla.banco.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cuentabancaria {

    private Integer nro_cuenta;
    private String tipo_cuenta;
    private Boolean active;

}
