package com.nttdata.jjla.cuentabancaria.service;

import com.nttdata.jjla.cuentabancaria.dto.CuentabancariaDto;
import com.nttdata.jjla.cuentabancaria.entity.Cuentabancaria;

import java.util.List;

public interface ICuentabancariaService {

    CuentabancariaDto registrar(CuentabancariaDto cuentabancariaDto);
    CuentabancariaDto actualizar(Long id, CuentabancariaDto cuentabancariaDto);
    List<CuentabancariaDto> listar();
    String eliminar(Long id);
    CuentabancariaDto buscar(Long id);

    List<Cuentabancaria> obtenerPorBancoId(Long bancoId);
    List<Cuentabancaria> obtenerPorClienteId(Long clienteId);

}
