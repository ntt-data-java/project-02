package com.nttdata.jjla.cuentabancaria.service;

import com.nttdata.jjla.cuentabancaria.dto.CuentabancariaDto;
import com.nttdata.jjla.cuentabancaria.entity.Cuentabancaria;
import com.nttdata.jjla.cuentabancaria.repository.CuentabancariaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CuentabancariaServiceImpl implements ICuentabancariaService {

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    CuentabancariaRepository cuentabancariaRepository;

    @Override
    public CuentabancariaDto registrar(CuentabancariaDto cuentabancariaDto) {
        Cuentabancaria cuentabancaria = cuentabancariaRepository.save(mapearEntidad(cuentabancariaDto));
        return mapearDTO(cuentabancaria);
    }

    @Override
    public CuentabancariaDto actualizar(Long id, CuentabancariaDto cuentabancariaDto) {
        Cuentabancaria cuentabancaria = cuentabancariaRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        cuentabancaria.setTipo_cuenta(cuentabancariaDto.getTipo_cuenta());
        cuentabancaria.setNro_cuenta(cuentabancariaDto.getNro_cuenta());
        cuentabancaria.setActive(cuentabancariaDto.getActive());
        cuentabancaria.setBancoId(cuentabancariaDto.getBancoId());
        cuentabancaria.setClienteId(cuentabancariaDto.getClienteId());
        Cuentabancaria actualizado = cuentabancariaRepository.save(cuentabancaria);
        return mapearDTO(actualizado);
    }

    @Override
    public List<CuentabancariaDto> listar() {
        List<Cuentabancaria> cuentabancarias = cuentabancariaRepository.findAll();
        return cuentabancarias.stream().map(cuentabancaria -> mapearDTO(cuentabancaria)).collect(Collectors.toList());
    }

    @Override
    public String eliminar(Long id) {
        Cuentabancaria cuentabancaria = cuentabancariaRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        if (Objects.nonNull(cuentabancaria)) {
            cuentabancariaRepository.deleteById(id);
            return "Eliminacion exitosa";
        }
        return "No existe el registro";
    }

    @Override
    public CuentabancariaDto buscar(Long id) {
        Optional<Cuentabancaria> cuentaFound = cuentabancariaRepository.findById(id);
        return cuentaFound.map(cuentabancaria ->mapearDTO(cuentabancaria)).orElseThrow(() -> new RuntimeException("Valor no presente"));
    }

    @Override
    public List<Cuentabancaria> obtenerPorBancoId(Long bancoId) {
        return cuentabancariaRepository.findByBancoId(bancoId);
    }

    @Override
    public List<Cuentabancaria> obtenerPorClienteId(Long clienteId) {
        return cuentabancariaRepository.findByClienteId(clienteId);
    }


    /*************** MAPEO CON MODELMAPPER *******************/
    private Cuentabancaria mapearEntidad(CuentabancariaDto cuentabancariaDto){
        Cuentabancaria entidadMapeada = modelMapper.map(cuentabancariaDto, Cuentabancaria.class);
        return entidadMapeada;
    }
    private CuentabancariaDto mapearDTO(Cuentabancaria cuentabancaria){
        CuentabancariaDto DTOmapeado = modelMapper.map(cuentabancaria, CuentabancariaDto.class);
        return DTOmapeado;
    }
}