package com.nttdata.jjla.cuentabancaria.repository;

import com.nttdata.jjla.cuentabancaria.entity.Cuentabancaria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CuentabancariaRepository extends JpaRepository<Cuentabancaria, Long> {

    List<Cuentabancaria> findByBancoId(Long bancoId);
    List<Cuentabancaria> findByClienteId(Long clienteId);

}
