package com.nttdata.jjla.cuentabancaria.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CuentabancariaDto {

    private Long id;
    private Integer nro_cuenta;
    private String tipo_cuenta;
    private Boolean active;
    private Long bancoId;
    private Long clienteId;

}
