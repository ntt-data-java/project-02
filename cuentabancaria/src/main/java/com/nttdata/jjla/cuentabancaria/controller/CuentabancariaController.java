package com.nttdata.jjla.cuentabancaria.controller;

import com.nttdata.jjla.cuentabancaria.dto.CuentabancariaDto;
import com.nttdata.jjla.cuentabancaria.entity.Cuentabancaria;
import com.nttdata.jjla.cuentabancaria.service.CuentabancariaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cuenta")
public class CuentabancariaController {

    @Autowired
    CuentabancariaServiceImpl cuentabancariaService;

    @GetMapping("/listar")
    public List<CuentabancariaDto> listar() {
        return cuentabancariaService.listar();
    }

    @PostMapping("/registrar")
    public ResponseEntity<CuentabancariaDto> registrar(@RequestBody CuentabancariaDto cuentaBancariaDto) {
        return new ResponseEntity<>(cuentabancariaService.registrar(cuentaBancariaDto), HttpStatus.CREATED);
    }

    @GetMapping("/buscar/{id}")
    public CuentabancariaDto buscar(@PathVariable(value = "id") Long id) {
        return cuentabancariaService.buscar(id);
    }

    @PutMapping("/actualizar/{id}")
    public CuentabancariaDto actualizar(@PathVariable Long id, @RequestBody CuentabancariaDto cuentaBancariaDto){
        return cuentabancariaService.actualizar(id, cuentaBancariaDto);
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<String> eliminar( @PathVariable(value = "id") Long id){
        cuentabancariaService.eliminar(id);
        return new ResponseEntity<>("Elemento eliminado !!!", HttpStatus.OK);
    }

    //************************* PARA PROBAR EL REST TEMPLATE START *******************************
    @GetMapping("/banco/{bancoId}")
    public ResponseEntity<List<Cuentabancaria>> listarCuentasPorBancoId(@PathVariable("bancoId") Long id){
        List<Cuentabancaria> cuentabancarias = cuentabancariaService.obtenerPorBancoId(id);
        if (cuentabancarias.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(cuentabancarias);
    }

    @GetMapping("/cliente/{clienteId}")
    public ResponseEntity<List<Cuentabancaria>> listarCuentasPorClienteId(@PathVariable("clienteId") Long id){
        List<Cuentabancaria> cuentabancarias = cuentabancariaService.obtenerPorClienteId(id);
        if (cuentabancarias.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(cuentabancarias);
    }
    //************************* PARA PROBAR EL REST TEMPLATE END *******************************

}
