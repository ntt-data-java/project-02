package com.nttdata.jjla.cliente.controller;

import com.nttdata.jjla.cliente.dto.ClienteDto;
import com.nttdata.jjla.cliente.model.Cuentabancaria;
import com.nttdata.jjla.cliente.model.Usuario;
import com.nttdata.jjla.cliente.service.ClienteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    ClienteServiceImpl clienteService;

    @GetMapping("/listar")
    public List<ClienteDto> listar() {
        return clienteService.listar();
    }

    @PostMapping("/registrar")
    public ClienteDto registrar(@RequestBody ClienteDto clienteDto) {
        return clienteService.registrar(clienteDto);
    }

    @PutMapping("/actualizar/{id}")
    public ClienteDto actualizar(@PathVariable Long id,@RequestBody ClienteDto clienteDto){
        return clienteService.actualizar(id, clienteDto);
    }

    @GetMapping("/buscar/{id}")
    public ClienteDto buscar(@PathVariable("id") long id){
        return clienteService.buscar(id);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id) {
        return clienteService.eliminar(id);
    }

    // *************************** REST TEMPLATE *************************
    @GetMapping("/cuenta/{clienteId}")
    public ResponseEntity<List<Cuentabancaria>> obtenerCuentaBancaria(@PathVariable("clienteId") Long clienteId){
        ClienteDto cliente = clienteService.buscar(clienteId);
        if (cliente == null) {
            return ResponseEntity.notFound().build();
        }
        List<Cuentabancaria> cuentaBancarias = clienteService.obtenerCuentasBancarias(clienteId);
        return ResponseEntity.ok(cuentaBancarias);
    }

    @GetMapping("/usuario/{usuarioId}")
    public ResponseEntity<List<Usuario>> obtenerUsuario(@PathVariable("usuarioId") Long usuarioId){
        ClienteDto cliente = clienteService.buscar(usuarioId);
        if (cliente == null) {
            return ResponseEntity.notFound().build();
        }
        List<Usuario> usuarios = clienteService.obtenerUsuarios(usuarioId);
        return ResponseEntity.ok(usuarios);
    }

}
