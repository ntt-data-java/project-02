package com.nttdata.jjla.cliente.service;

import com.nttdata.jjla.cliente.dto.ClienteDto;
import com.nttdata.jjla.cliente.model.Cuentabancaria;
import com.nttdata.jjla.cliente.model.Usuario;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IClienteService {

    ClienteDto registrar(ClienteDto clienteDto);
    ClienteDto actualizar(Long id, ClienteDto clienteDto);
    List<ClienteDto> listar();
    String eliminar(Long id);
    ClienteDto buscar(Long id);

    List<Cuentabancaria> obtenerCuentasBancarias(Long clienteId);
    List<Usuario> obtenerUsuarios(Long usuarioId);

}
