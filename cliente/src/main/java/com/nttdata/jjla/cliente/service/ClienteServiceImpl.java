package com.nttdata.jjla.cliente.service;

import com.nttdata.jjla.cliente.dto.ClienteDto;
import com.nttdata.jjla.cliente.entity.Cliente;
import com.nttdata.jjla.cliente.model.Cuentabancaria;
import com.nttdata.jjla.cliente.model.Usuario;
import com.nttdata.jjla.cliente.repository.ClienteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClienteServiceImpl implements IClienteService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    ClienteRepository clienteRepository;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public ClienteDto registrar(ClienteDto clienteDto) {
        Cliente registrado = clienteRepository.save(mapearEntidad(clienteDto));
        return mapearDTO(registrado);
    }

    @Override
    public ClienteDto actualizar(Long id, ClienteDto clienteDto) {
        Cliente cliente = clienteRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        cliente.setNombre(clienteDto.getNombre());
        cliente.setApellidos(clienteDto.getApellidos());
        cliente.setSexo(clienteDto.getSexo());
        cliente.setActive(clienteDto.getActive());
        Cliente actualizado = clienteRepository.save(cliente);
        return mapearDTO(actualizado);
    }

    @Override
    public List<ClienteDto> listar() {
        List<Cliente> clientes = clienteRepository.findAll();
        return clientes.stream().map(cliente -> mapearDTO(cliente)).collect(Collectors.toList());
    }

    @Override
    public String eliminar(Long id) {
        Cliente cliente = clienteRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        if (Objects.nonNull(cliente)) {
            clienteRepository.deleteById(id);
            return "Eliminacion exitosa";
        }
        return "No existe el registro";
    }

    @Override
    public ClienteDto buscar(Long id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        return clienteOptional.map(cliente ->mapearDTO(cliente)).orElseThrow(() -> new RuntimeException("Valor no presente"));
    }

    // ************************** USO DE REST TEMPLATE **********************
    @Override
    public List<Cuentabancaria> obtenerCuentasBancarias(Long clienteId) {
        List<Cuentabancaria> cuentaBancarias = restTemplate.getForObject("http://localhost:8004/api/cuenta/cliente/" + clienteId, List.class);
        return cuentaBancarias;
    }

    @Override
    public List<Usuario> obtenerUsuarios(Long usuarioId) {
        List<Usuario> cuentaBancarias = restTemplate.getForObject("http://localhost:8002/api/usuario/cliente/" + usuarioId, List.class);
        return cuentaBancarias;
    }


    /************************* MAPEO CON MODELMAPPER *******************/
    private Cliente mapearEntidad(ClienteDto clienteDto){
        Cliente entidadMapeada = modelMapper.map(clienteDto, Cliente.class);
        return entidadMapeada;
    }
    private ClienteDto mapearDTO(Cliente cliente){
        ClienteDto DTOmapeado = modelMapper.map(cliente, ClienteDto.class);
        return DTOmapeado;
    }

}
