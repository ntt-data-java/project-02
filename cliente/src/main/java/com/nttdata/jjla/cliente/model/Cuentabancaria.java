package com.nttdata.jjla.cliente.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cuentabancaria {

    private Integer nro_cuenta;
    private String tipo_cuenta;
    private Boolean active;
    private Long bancoId;
    private Long clienteId;

}
