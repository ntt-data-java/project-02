package com.nttdata.jjla.cliente.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {

    private String usuario;
    private String clave;
    private Boolean active;
    private Long empleadoId;

}
