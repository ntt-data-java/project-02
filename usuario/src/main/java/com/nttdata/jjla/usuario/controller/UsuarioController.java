package com.nttdata.jjla.usuario.controller;

import com.nttdata.jjla.usuario.dto.UsuarioDto;
import com.nttdata.jjla.usuario.entity.Usuario;
import com.nttdata.jjla.usuario.service.UsuarioServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioServiceImpl usuarioService;

    @GetMapping("/listar")
    public List<UsuarioDto> listar() {
        return usuarioService.listar();
    }

    @PostMapping("/registrar")
    public ResponseEntity<UsuarioDto> registrar(@RequestBody UsuarioDto usuarioDto) {
        return new ResponseEntity<>(usuarioService.registrar(usuarioDto), HttpStatus.CREATED);
    }

    @GetMapping("/buscar/{id}")
    public UsuarioDto buscar(@PathVariable(value = "id") Long id) {
        return usuarioService.buscar(id);
    }

    @PutMapping("/actualizar/{id}")
    public UsuarioDto actualizar(@PathVariable Long id, @RequestBody UsuarioDto usuarioDto){
        return usuarioService.actualizar(id, usuarioDto);
    }

    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<String> eliminar( @PathVariable(value = "id") Long id){
        usuarioService.eliminar(id);
        return new ResponseEntity<>("Elemento eliminado !!!", HttpStatus.OK);
    }

    //************************* PARA PROBAR EL REST TEMPLATE START *******************************
    @GetMapping("/cliente/{clienteId}")
    public ResponseEntity<List<Usuario>> listarClientesPorClienteId(@PathVariable("clienteId") Long id){
        List<Usuario> usuarios = usuarioService.obtenerPorClienteId(id);
        if (usuarios.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(usuarios);
    }
    //************************* PARA PROBAR EL REST TEMPLATE END *******************************

}
