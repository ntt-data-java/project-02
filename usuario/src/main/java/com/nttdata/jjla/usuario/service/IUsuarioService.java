package com.nttdata.jjla.usuario.service;

import com.nttdata.jjla.usuario.dto.UsuarioDto;
import com.nttdata.jjla.usuario.entity.Usuario;

import java.util.List;

public interface IUsuarioService {

    UsuarioDto registrar(UsuarioDto usuarioDto);
    UsuarioDto actualizar(Long id, UsuarioDto usuarioDto);
    List<UsuarioDto> listar();
    String eliminar(Long id);
    UsuarioDto buscar(Long id);

    List<Usuario> obtenerPorClienteId(Long clienteId);

}
