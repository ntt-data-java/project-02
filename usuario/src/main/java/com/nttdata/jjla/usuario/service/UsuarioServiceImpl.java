package com.nttdata.jjla.usuario.service;

import com.nttdata.jjla.usuario.dto.UsuarioDto;
import com.nttdata.jjla.usuario.entity.Usuario;
import com.nttdata.jjla.usuario.repository.UsuarioRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements IUsuarioService{

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public UsuarioDto registrar(UsuarioDto usuarioDto) {
        Usuario usuario = usuarioRepository.save(mapearEntidad(usuarioDto));
        return mapearDTO(usuario);
    }

    @Override
    public UsuarioDto actualizar(Long id, UsuarioDto usuarioDto) {
        return null;
    }

    @Override
    public List<UsuarioDto> listar() {
        List<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios.stream().map(user -> mapearDTO(user)).collect(Collectors.toList());
    }

    @Override
    public String eliminar(Long id) {
        Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new RuntimeException("Valor no presente"));
        if (Objects.nonNull(usuario)) {
            usuarioRepository.deleteById(id);
            return "Eliminacion exitosa";
        }
        return "No existe el registro";
    }

    @Override
    public UsuarioDto buscar(Long id) {
        Optional<Usuario> userFound = usuarioRepository.findById(id);
        return userFound.map(user ->mapearDTO(user)).orElseThrow(() -> new RuntimeException("Valor no presente"));
    }

    @Override
    public List<Usuario> obtenerPorClienteId(Long clienteId) {
        return usuarioRepository.findByClienteId(clienteId);
    }

    /*************** MAPEO CON MODELMAPPER *******************/
    private Usuario mapearEntidad(UsuarioDto usuarioDto){
        Usuario entidadMapeada = modelMapper.map(usuarioDto, Usuario.class);
        return entidadMapeada;
    }
    private UsuarioDto mapearDTO(Usuario usuario){
        UsuarioDto DTOmapeado = modelMapper.map(usuario, UsuarioDto.class);
        return DTOmapeado;
    }

}
