package com.nttdata.jjla.usuario.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDto {

    private Long id;
    private String usuario;
    private String clave;
    private Boolean active;
    private Long clienteId;

}
